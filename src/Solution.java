import java.util.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    static String SOUTH = "SOUTH";
    static String EAST = "EAST";
    static String NORTH = "NORTH";
    static String WEST = "WEST";
    static String LOOP = "LOOP";
    static int NUMBER_OF_DIRECTION = 4;

    static String[] DIRECTION = {SOUTH, EAST, NORTH, WEST};
    static int[][] move = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
    static int SOUTH_DIRECTION = 0;
    static int EAST_DIRECTION = 1;
    static int NORTH_DIRECTION = 2;
    static int WEST_DIRECTION = 3;
    static int LOOP_LIMIT = 100;

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);

        CityMap cityMap = createCityMap(in);

        cityMap.exploreBenderPath();

        cityMap.followBender();

    }

    static CityMap createCityMap(Scanner in) {
        int numberOfLines = in.nextInt();
        int numberOfColumns = in.nextInt();
        System.err.println(numberOfLines + " " + numberOfColumns);
        in.nextLine();
        CityMap cityMap = new CityMap(numberOfLines, numberOfColumns);
        int teleportationIndex = 0;
        for (int row = 0; row < numberOfLines; row++) {
            String lines = in.nextLine();
            System.err.println(lines);
            for (int column = 0; column < numberOfColumns; column++) {
                cityMap.cells[row][column].cellType = lines.charAt(column);
                if (cityMap.cells[row][column].isStart()) {
                    cityMap.bender.position = cityMap.cells[row][column];
                }
                if (cityMap.cells[row][column].isTeleporter()) {
                    cityMap.teleportation[teleportationIndex++] = cityMap.cells[row][column];
                }
            }
        }
        return cityMap;
    }

    static class CityMap {
        int numberOfLines;
        int numberOfColumns;
        Cell[][] cells;
        Bender bender;
        Cell[] teleportation;
        int teleporteIndex = 0;
        int loop;
        State state;

        public CityMap(int numberOfLines, int numberOfColumns) {
            this.numberOfLines = numberOfLines;
            this.numberOfColumns = numberOfColumns;
            this.bender = new Bender();
            this.teleportation = new Cell[2];
            initializeCells();
        }

        void initializeCells() {
            this.cells = new Cell[numberOfLines][numberOfColumns];
            for (int row = 0; row < numberOfLines; row++) {
                for (int column = 0; column < numberOfColumns; column++) {
                    cells[row][column] = new Cell(row, column);
                }
            }
        }

        boolean isInMap(int x, int y) {
            return x >= 0 && x < numberOfLines && y >= 0 && y < numberOfColumns;
        }

        Cell nextCell() {
            int[] xy = move[bender.direction];
            int nextX = bender.position.x + xy[0];
            int nextY = bender.position.y + xy[1];
            if (!isInMap(nextX, nextY)) {
                return null;
            }
            return cells[nextX][nextY];
        }

        boolean isValid(Cell cell) {
            if (cell.isUnBreakableObstacle()) {
                return false;
            }
            if (cell.isBreakableObstacle()) {
                if (!bender.breakerMode) {
                    return false;
                }
            }
            return true;
        }

        Cell findNextValidCell() {
            Cell nextCell = null;
            for (int direction = 0; direction < NUMBER_OF_DIRECTION; direction++) {
                int dir = direction;
                if (bender.invertMode) {
                    dir = 3 - direction;
                }
                bender.changeDirection(dir);
                nextCell = nextCell();
                if (nextCell != null && isValid(nextCell)) {
                    break;
                }
            }
            return nextCell;
        }

        void computeNextPosition() {
            boolean breakerMode = bender.breakerMode;
            boolean invertMode = bender.invertMode;
            bender.updateState();
            Cell nextPosition = nextCell();
            if (nextPosition != null) {
                if (nextPosition.isUnBreakableObstacle()) {
                    nextPosition = findNextValidCell();
                } else if (nextPosition.isBreakableObstacle()) {
                    if (!bender.breakerMode) {
                        nextPosition = findNextValidCell();
                    } else {
                        nextPosition.breakObstacle();
                    }
                }
                if (nextPosition == null) {
                    System.err.println("Big Problem....");
                }
                State tmp = state;
                state = new State(nextPosition, breakerMode, invertMode);
                state.prev = tmp;
                this.state.direction = DIRECTION[bender.direction];
                if (nextPosition.hasLoop(state)) {
                    loop++;
                } else {
                    nextPosition.setState(state);
                }
                if (nextPosition.isTeleporter()) {
                    if (nextPosition.equals(teleportation[teleporteIndex])) {
                        teleporteIndex = 1 - teleporteIndex;
                    }
                    nextPosition = teleportation[teleporteIndex];
                }
                bender.position = nextPosition;
            }
        }

        void printState() {
            State state = this.state;
            String out = "";
            while (state != null) {
                if (state.direction != null) {
                    if (out.isEmpty()) {
                        out = state.direction;
                    } else {
                        out = state.direction + "\n" + out;
                    }
                }
                state = state.prev;
            }
            System.out.println(out);
        }

        @Override
        public String toString() {
            String out = "";
            for (int i = 0; i < numberOfLines; i++) {
                for (int j = 0; j < numberOfColumns; j++) {
                    out += cells[i][j].cellType;
                }
                out += "\n";
            }
            return out;
        }

        void exploreBenderPath() {
            while (!bender.position.isSuicide() && loop < LOOP_LIMIT) {
                computeNextPosition();
            }
        }

        void followBender() {
            if (loop == LOOP_LIMIT) {
                System.out.println(LOOP);
            } else {
                printState();
            }
        }

    }

    static class State {
        int x;
        int y;
        char cellType;
        String direction;
        boolean beerMode;
        boolean invertMode;
        State prev;

        public State(Cell cell, boolean beerMode, boolean invertMode) {
            this.x = cell.x;
            this.y = cell.y;
            this.cellType = cell.cellType;
            this.beerMode = beerMode;
            this.invertMode = invertMode;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            State state = (State) o;

            if (x != state.x) return false;
            if (y != state.y) return false;
            if (cellType != state.cellType) return false;
            if (beerMode != state.beerMode) return false;
            if (invertMode != state.invertMode) return false;
            return true;

        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            result = 31 * result + (int) cellType;
            result = 31 * result + (beerMode ? 1 : 0);
            result = 31 * result + (invertMode ? 1 : 0);
            return result;
        }
    }

    static class Cell {
        int x;
        int y;
        State state;

        char cellType; //

        Cell(int x, int y) {
            this.x = x;
            this.y = y;
        }

        boolean isUnBreakableObstacle() {
            return cellType == '#';
        }

        boolean isBreakableObstacle() {
            return cellType == 'X';
        }

        boolean isBeer() {
            return cellType == 'B';
        }

        boolean isTeleporter() {
            return cellType == 'T';
        }

        boolean isInversion() {
            return cellType == 'I';
        }

        boolean isStart() {
            return cellType == '@';
        }

        boolean isSuicide() {
            return cellType == '$';
        }

        boolean isSouthDirection() {
            return cellType == 'S';
        }

        boolean isEastDirection() {
            return cellType == 'E';
        }

        boolean isNorthDirection() {
            return cellType == 'N';
        }

        boolean isWestDirection() {
            return cellType == 'W';
        }

        @Override
        public String toString() {
            return cellType + "(" + x + ", " + y + ")";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Cell cell = (Cell) o;

            if (x != cell.x) return false;
            if (y != cell.y) return false;
            return cellType == cell.cellType;

        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            result = 31 * result + (int) cellType;
            return result;
        }

        boolean hasLoop(State state) {
            if (this.state == null) {
                return false;
            }
            return this.state.equals(state);
        }

        void setState(State state) {
            this.state = state;
        }

        void breakObstacle() {
            if (isBreakableObstacle()) {
                cellType = ' ';
            }
        }
    }

    static class Bender {
        Cell position;
        int direction = 0;
        boolean breakerMode = false;
        boolean invertMode = false;

        void changeDirection(int direction) {
            this.direction = direction;
        }

        void updateState() {
            if (position.isBeer()) {
                breakerMode = !breakerMode;
            }
            if (position.isInversion()) {
                invertMode = !invertMode;
            } else if (position.isSouthDirection()) {
                changeDirection(SOUTH_DIRECTION);
            } else if (position.isEastDirection()) {
                changeDirection(EAST_DIRECTION);
            } else if (position.isNorthDirection()) {
                changeDirection(NORTH_DIRECTION);
            } else if (position.isWestDirection()) {
                changeDirection(WEST_DIRECTION);
            }
        }
    }
}